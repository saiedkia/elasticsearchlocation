﻿using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ESUtil
{
    public class Helper
    {
        readonly ElasticClient _esClient;
        readonly string indexName = "customer_index";

        public Helper()
        {
            var node = new Uri("http://localhost:9200");
            var settings = new ConnectionSettings(node);
            _esClient = new ElasticClient(settings);
        }

        public bool CleanIndexes()
        {
            if (_esClient.IndexExists(new IndexExistsRequest(indexName)).Exists)
                return _esClient.DeleteIndex(new DeleteIndexRequest(indexName)).ServerError == null;

            return true;
        }

        public bool CreateIndexAndMapping()
        {
            return _esClient.CreateIndex(indexName, mapings =>
              mapings.Mappings(map => map.Map<Customer>(prop =>
              prop.Properties(p => p.GeoPoint(l => l.Name(n => n.Location)))))).ServerError == null;

            //_esClient.CreateIndex(indexName, mapings =>
            //mapings.Mappings(map => map.Map<Customer>(prop =>
            //prop.Properties(p => p.Object<Location>(l =>
            //{
            //    return l.Name(n => n.Location)
            //    .Properties(mp => mp.GeoPoint(g => g.Name(gLat => gLat.lat)).GeoPoint(gl => gl.Name(n => n.lon)));
            //    //.Properties(mpl => mpl.GeoPoint(gl => gl.Name(glon => glon.lon)));
            //})))));
        }



        public async Task<bool> AddCustomer(Customer customer)
        {
            IIndexResponse response = await _esClient.IndexAsync(customer, id => id.Index(indexName));

            return response.ServerError == null && response.Result != Result.Error;
        }

        public async Task AddCustomers(IEnumerable<Customer> customers)
        {
            foreach (Customer customer in customers)
            {
                await AddCustomer(customer);
            }
        }

        public async Task<List<Customer>> GetAllCustomers()
        {
            ISearchResponse<Customer> response = await _esClient.SearchAsync<Customer>(search =>
                search.Index(indexName).Size(50).Query(query => query.MatchAll()));

            return response.ServerError == null ? response.Documents.ToList() : new List<Customer>();
        }


        public async Task<List<Customer>> FindByDistance(double lat, double lon, double distance)
        {
            ISearchResponse<Customer> response = await _esClient.SearchAsync<Customer>(search =>
                search.Index(indexName).Query(query => query.GeoDistance(loc => loc.Field(x => x.Location)
                .Location(lat, lon).Distance(distance, DistanceUnit.Meters))));

            return response.ServerError == null ? response.Documents.ToList() : new List<Customer>();
        }

        public async Task<List<Customer>> FindByRadius(double lat, double lon, double radius)
        {
            ISearchResponse<Customer> response = await _esClient.SearchAsync<Customer>(search =>
                search.Index(indexName).Query(query => query.GeoShapeCircle(loc => loc.Field(x => x.Location)
                .Coordinates(lat, lon).Radius(radius.ToString()))));

            return response.ServerError == null ? response.Documents.ToList() : new List<Customer>();
        }
    }
}
