﻿namespace ESUtil
{
    public class Customer
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public Location Location { get; set; }
    }


    public class Location
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}
