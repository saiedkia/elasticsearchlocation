using ESUtil;
using FluentAssertions;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Xunit;

namespace LocationTest
{
    public class UnitOne
    {
        Helper _helper = new Helper();
        List<Customer> _customers;

        const double LAT = 88.0;
        const double LON = 57.0;
        const int ID = 999;
        const int CUSTOMERS_COUNT = 20;

        [Fact]
        public void clear_all_es_data_and_create_geo_mapping()
        {
            bool delResult = _helper.CleanIndexes();
            bool indexResult = _helper.CreateIndexAndMapping();

            delResult.Should().BeTrue();
            indexResult.Should().BeTrue();
        }


        [Fact]
        public void add_customers_succesfully_in_es()
        {
            _customers = LocationGenerator.GenerateRandomData(CUSTOMERS_COUNT);
            _helper.AddCustomers(_customers).Wait();
            Thread.Sleep(4000);
        }

        [Fact]
        public void read_all_customers()
        {
            int expected = CUSTOMERS_COUNT;
            int result = _helper.GetAllCustomers().Result.Count;
            result.Should().Be(expected);
        }


        [Fact]
        public void should_get_hard_coded_customer()
        {
            Customer customer = new Customer()
            {
                Age = ID,
                Id = 9999999,
                Name = "hard coded user",
                Location = new Location() { Latitude = LAT, Longitude = LON }
            };

            _helper.AddCustomer(customer).Wait();

            List<Customer> customers = _helper.FindByDistance(LAT, LON, 20).Result;
            customers.Should().HaveCount(1);
            customers.First().Age.Should().Equals(ID);
        }

    }
}
