﻿using Bogus;
using ESUtil;
using System.Collections.Generic;

namespace LocationTest
{
    public static class LocationGenerator
    {
        public static List<Customer> GenerateRandomData(int length)
        {
            List<Customer> customers = new List<Customer>();
            for (int i = 0; i < length; i++)
            {
                Customer customer = new Faker<Customer>()
                    .RuleFor(x => x.Age, (f, c) => f.Random.Int(10, 99))
                    .RuleFor(x => x.Id, (f, c) => f.UniqueIndex)
                    .RuleFor(x => x.Name, (f, c) => f.Name.Random.String(1, 30))
                    .RuleFor(x => x.Location, (f, c) => new Location()
                    {
                        Latitude = f.Random.Double(55, 60),
                        Longitude = f.Random.Double(53, 60),
                    });

                customers.Add(customer);
            }


            return customers;
        }
    }
}
